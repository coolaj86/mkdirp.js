'use strict';

var crypto = require('crypto');
var os = require('os');
var fs = require('fs');
var path = require('path');
var tmpdir = os.tmpdir();
var dirs = [tmpdir].concat([ 'foo', 'bar', 'baz', 'quux' ]);
var rnds = [tmpdir].concat([
  crypto.randomBytes(4).toString('hex')
, crypto.randomBytes(4).toString('hex')
, crypto.randomBytes(4).toString('hex')
, crypto.randomBytes(4).toString('hex')
]);

var mkdirp = require('./mkdirp.js');

fs.stat(rnds.join(path.sep), function (err) {
  if (!err) { throw new Error("Sanity error: random directory already exists"); }
  mkdirp(dirs.join(path.sep), function (err) {
    if (err) { throw err; }
    mkdirp(dirs.join(path.sep), function (err) {
      if (err) { throw err; }
      mkdirp(rnds.join(path.sep), function (err) {
        if (err) { throw err; }
        fs.stat(rnds.join(path.sep), function (err, stat) {
          if (err) { throw err; }
          if (!stat.isDirectory()) { throw new Error("path is not a directory"); }
          mkdirp(rnds.join(path.sep), function (err) {
            if (err) { throw err; }
            mkdirp('/root/special/place', function (err) {
              if (!err) { throw new Error("either you ran the test as root (don't do that)"
                + " or there was an unreported error"); }
              dirs.shift(); // local dir
              mkdirp(dirs.join(path.sep), function (err) {
                console.info("PASS");
              });
            });
          });
        });
      });
    });
  });
});
