'use strict';

var util = require('util');
//var mkdirp = util.promisify(require('@root/mkdirp'));
var mkdirp = util.promisify(require('./'));

mkdirp('/tmp/path/to/whatever').then(function () {
  console.info("directory now exists");
}).catch(function (err) {
  console.error(err);
});
